# -*- coding: utf-8 -*-

import os
import signal
import simple_http_server.logger as logger
import simple_http_server.server as server

logger.set_level("DEBUG")
_logger = logger.get_logger("__main__")


def on_sig_term(signum, frame):
    _logger.info(f"Receive signal [{signum}], stop server now...")
    server.stop()


def main(*args):
    root = os.path.dirname(os.path.abspath(__file__))
    signal.signal(signal.SIGTERM, on_sig_term)
    signal.signal(signal.SIGINT, on_sig_term)
    server.scan("demo", r'.*controllers.*')
    server.start(
        port=9090,
        resources={"/public/*": f"{root}/static/html"})


if __name__ == "__main__":
    main()
