# -*- coding: utf-8 -*-

from simple_http_server import WebsocketHandler, WebsocketRequest, WebsocketSession, websocket_handler
import simple_http_server.logger as logger

_logger = logger.get_logger("websocket")


@websocket_handler(endpoint="/ws/{path_val}")
class WSHandler(WebsocketHandler):

    def on_handshake(self, request: WebsocketRequest):
        path_val = request.path_values['path_val']
        _logger.info(f"on handshake, path val :: {path_val}")
        if path_val == "err":
            return 400, {"Self Defind Header": "Error!"}
        return None

    def on_open(self, session: WebsocketSession):

        _logger.info(f">>{session.id}<< open! {session.request.path_values}")

    def on_text_message(self, session: WebsocketSession, message: str):
        _logger.info(f">>{session.id}<< on text message: {message}")
        session.send(message)

    def on_close(self, session: WebsocketSession, reason: str):
        _logger.info(f">>{session.id}<< close::{reason}")
