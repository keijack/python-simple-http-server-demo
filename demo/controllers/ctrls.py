# -*- coding: utf-8 -*-

from typing import List
import uuid
from simple_http_server import ModelDict, Redirect, RegGroup, RegGroups
from simple_http_server import Headers
from simple_http_server import StaticFile
from simple_http_server import HttpError
from simple_http_server import JSONBody
from simple_http_server import Header
from simple_http_server import Parameters
from simple_http_server import Cookie
from simple_http_server import Cookies
from simple_http_server import PathValue
from simple_http_server import Parameter
from simple_http_server import MultipartFile
from simple_http_server import Response
from simple_http_server import Request
from simple_http_server import Session
from simple_http_server import filter_map
from simple_http_server import request_map, route
from simple_http_server import controller
import os
import simple_http_server.logger as logger
import simple_http_server.server as server


__logger = logger.get_logger("ctrls")


@request_map("/")
@request_map("/index")
def my_ctrl():
    return {"code": 0, "message": "success"}  # You can return a dictionary, a string or a `simple_http_server.simple_http_server.Response` object.


@request_map("/say_hello", method=["GET", "POST"])
def my_ctrl2(name, name2=Parameter("name", default="KEIJACK")):
    """name and name2 is the same"""
    return f"<!DOCTYPE html><html><body>hello, {name}, {name2}</body></html>"


@request_map("/error")
def my_ctrl3():
    return Response(status_code=500)


@request_map("/exception")
def exception_ctrl():
    raise HttpError(400, "Exception")


@request_map("/upload", method="POST")
def my_upload(img: MultipartFile, file_name: Parameter = Parameter("pic_name", required=False, default="")):

    root = os.path.dirname(os.path.abspath(__file__))
    target_folder = f"{root}/upload"
    if not os.path.isdir(target_folder):
        os.makedirs(target_folder)
    img.save_to_file(f"{target_folder}/{file_name}")
    return f"<!DOCTYPE html><html><body>upload ok! {file_name} </body></html>"

@request_map("/upload", method="GET")
def show_upload():
    """
    " 仅做演示，由于已经将 {root}/static/ 映射成静态文件，你可以通过 /public/index.html 来访问同一个文件。
    """
    root = os.path.dirname(os.path.abspath(__file__))
    return StaticFile(f"{root}/static/index.html", "text/html; charset=utf-8")


@request_map("/post_json", method="POST")
def post_json(json=JSONBody()):
    print(json)
    return json


@request_map("/headers")
def set_headers(res: Response, headers: Headers, cookies: Cookies, cookie=Cookie("sc")):
    print("==================cookies==========")
    print(cookies)
    print("==================cookies==========")
    print(cookie)
    res.add_header("Set-Cookie", "sc=keijack; Expires=Web, 31 Oct 2018 00:00:00 GMT;")
    res.add_header("Set-Cookie", "sc=keijack2;")
    res.body = "<!DOCTYPE html><html><body>OK!</body></html>"


@request_map("tuple")
def tuple_results():
    return 200, Headers({"my-header": "headers"}), {"success": "成功！"}


@request_map("session")
def test_session(session: Session, invalid=False):
    ins = session.get_attribute("in-session")
    if not ins:
        session.set_attribute("in-session", "Hello, Session!")

    __logger.info("session id: %s" % session.id)
    if invalid:
        __logger.info("session[%s] is being invalidated. " % session.id)
        session.invalidate()
    return "<!DOCTYPE html><html><body>%s</body></html>" % str(ins)


@request_map("tuple_cookie")
def tuple_with_cookies(headers=Headers(), all_cookies=Cookies(), cookie_sc=Cookie("sc")):
    print("=====>headers")
    print(headers)
    print("=====> cookies ")
    print(all_cookies)
    print("=====> cookie sc ")
    print(cookie_sc)
    print("======<")
    import datetime
    expires = datetime.datetime(2018, 12, 31)

    cks = Cookies()
    # cks = cookies.SimpleCookie() # you could also use the build-in cookie objects
    cks["ck1"] = "keijack"
    cks["ck1"]["path"] = "/"
    cks["ck1"]["expires"] = expires.strftime(Cookies.EXPIRE_DATE_FORMAT)

    return 200, Header({"xx": "yyy"}), cks, "<html><body>OK</body></html>"


@filter_map("^/tuple")
def filter_tuple(ctx):
    print("---------- through filter ---------------")
    # add a header to request header
    ctx.request.headers["filter-set"] = "through filter"
    if "user_name" not in ctx.request.parameter:
        ctx.response.send_redirect("/index")
    elif "pass" not in ctx.request.parameter:
        ctx.response.send_error(400, "pass should be passed")
        # you can also raise a HttpError
        # raise HttpError(400, "pass should be passed")
    else:
        # you should always use do_chain method to go to the next
        ctx.do_chain()


@request_map("/redirect")
def redirect():
    return Redirect("/index")


@request_map("/stop")
def stop():
    server.stop()
    return {"stopped": True}


@request_map("/params")
def my_ctrl4(user_name,
             password=Parameter(name="passwd", required=True),
             remember_me=True,
             locations=[],
             json_param={},
             lcs=Parameters(name="locals", required=True),
             content_type=Header("Content-Type", default="application/json"),
             connection=Header("Connection"),
             ua=Header("User-Agent"),
             headers=Headers()
             ):
    return f"""<!DOCTYPE html>
    <html>
    <head>
        <title>show all params!</title>
    </head>
    <body>
        <p>user_name: {user_name}</p>
        <p>password: {password}</p>
        <p>remember_me: {remember_me}</p>
        <p>locations: {locations}</p>
        <p>json_param: {json_param}</p>
        <p>locals: {lcs}</p>
        <p>conent_type: {content_type}</p>
        <p>user_agent: {ua}</p>
        <p>connection: {connection}</p>
        <p>all Headers: {headers}</p>
    </body>
    </html>
    """


@request_map("/int_status_code")
def return_int(status_code=200):
    return status_code


@request_map("/a/{pval}/{path_val}/x")
def my_path_val_ctr(pval: PathValue, path_val=PathValue()):
    __logger.info("val is %s , path_val is %s" % (pval, path_val))
    return "<html><body>%s, %s</body></html>" % (pval, path_val)


@controller(args=["my-ctr"], kwargs={"desc": "desc"})
@route("/obj")
class MyController:
    """
    " request_map 还有另一个别名为 @route 
    """

    def __init__(self, name, desc="") -> None:
        self._name = f"ctr object[#{name}-{uuid.uuid4().hex}]:{desc}"

    @route("/hello", method="GET, POST")
    @request_map
    def my_ctrl_mth(self, model: ModelDict):
        return {"message": f"hello, {model['name']}, {self._name} says. "}

    @request_map("/hello2", method=("GET", "POST"))
    def my_ctr_mth2(self, name: str, i: List[int]):
        return f"<html><head><title>{self._name}</title></head><body>{self._name}: {name}, {i}</body></html>"

    @route(regexp="^(reg/(.+))$", method="GET")
    def my_reg_ctr(self, reg_group: RegGroup = RegGroup(1)):
        return f"{self._name}, {reg_group.group},{reg_group}"
