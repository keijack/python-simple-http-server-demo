# Python-Simple-Http-Server 的演示工程

## 安装依赖

```python
python3 -m pip install -r requirements.txt
```

## 运行

```python
python3 main.py
```

## 开发

在开发过程中，如果你不想每次修改控制器就手动重启服务，你可以使用 `dev.py`，该脚本使用 `watchdog` 监听 `demo` 文件变化，然后重启服务。请注意，该脚本使用 `venv`，如你不使用 `venv`，或者自定义了虚拟环境的目录，请修改具体脚本参数。

```python
python3 -m venv venv
source ./venv/bin/activate
python3 -m pip install -r requirements.txt
python3 dev.py
```

## 说明 

执行之后会在前台启动一个端口为 `9090` 的 HTTP 服务器，你可以通过 `http://127.0.0.1:9090/` 来访问你的应用程序。具体可以访问的 url 可以参考 `./demo/controllers/ctrls.py` 中的 `@request_map` 编写的控制器方法。

该例子接收 websocket 请求的例子，具体代码参考 `./demo/controllers/websocket_handler.py`